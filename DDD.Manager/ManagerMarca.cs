﻿using DDD.Entidades;
using DDD.Negocios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Manager
{
	public class ManagerMarca
	{
		public List<Marca> Listar()
		{
			return new DomMarca().Listar();
		}
	}
}
