﻿using DDD.Entidades;
using DDD.Negocios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Manager
{
	public class ManagerAuto
	{
		public List<Auto> Listar()
		{
			return new DomAuto().Listar();
		}

		public int Insertar(Auto a)
		{
			return new DomAuto().Insertar(a);
		}

		public int Actualizar(Auto a)
		{
			return new DomAuto().Actualizar(a);
		}

		public Auto Obtener(int id)
		{
			return new DomAuto().Obtener(id);
		}

		public int Eliminar(int id)
		{
			return new DomAuto().Eliminar(id);
		}
	}
}
