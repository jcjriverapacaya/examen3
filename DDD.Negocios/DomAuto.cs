﻿using DDD.ADOSqlServer;
using DDD.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Negocios
{
	public class DomAuto
	{
		public List<string> Mensajes { get; set; }
		public List<Auto> Listar()
		{
			return new ADOSqlAuto().Listar();
		}

		public int Insertar(Auto a)
		{
			return new ADOSqlAuto().Insertar(a);
		}

		public int Actualizar(Auto a)
		{
			return new ADOSqlAuto().Actualizar(a);
		}

		public Auto Obtener(int id)
		{
			return new ADOSqlAuto().Obtener(id);
		}

		public int Eliminar(int id)
		{
			return new ADOSqlAuto().Eliminar(id);
		}

	}
}
