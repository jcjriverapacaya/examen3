﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Entidades
{
	public partial class Auto
	{
		[Key]
		[Display(Name = "Id")]
		public int idAuto { get; set; }

		[Display(Name = "Placa")]
		[Required(AllowEmptyStrings = false , ErrorMessage = "Ingresar Placa del Auto")]
		[MaxLength(20, ErrorMessage = "La placa del auto no debe exceder los 20 caracteres")]
		public string Placa { get; set; }

		[Display(Name = "Modelo")]
		[Required(AllowEmptyStrings = false, ErrorMessage = "Ingresar Modelo del Auto")]
		[MaxLength(50, ErrorMessage = "El Modelo del auto no debe exceder los 50 caracteres")]
		public string Modelo { get; set; }

		[Display(Name = "Año")]
		public int Año { get; set; }

		[Display(Name = "Marca")]
		public int idMarca { get; set; }

	}
}
