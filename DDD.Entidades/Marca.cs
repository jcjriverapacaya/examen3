﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Entidades
{
	public class Marca
	{
		public int idMarca { get; set; }
		public string Nombre { get; set; }
	}
}
