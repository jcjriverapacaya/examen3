﻿using DDD.Entidades;
using DDD.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Examen3.Controllers
{
    public class AutoController : Controller
    {
        // Definir un Manager
        ManagerAuto mngAuto = new ManagerAuto();
        ManagerMarca mngMarca = new ManagerMarca();
        // GET: Auto
        public ActionResult Index()
		{
            return View();
		}

        public ActionResult ListarAutos()
		{
            return View(mngAuto.Listar());
		}


        public ActionResult RegistrarAuto()
		{
            ViewBag.marcas = mngMarca.Listar();
            return View(new Auto());
		}


        [HttpPost]
        public ActionResult RegistrarAuto(Auto a)
		{
			if (!ModelState.IsValid)
			{
                ViewBag.marcas = mngMarca.Listar();
                return View(a);
			}
			else
			{
                mngAuto.Insertar(a);
                return RedirectToAction("ListarAutos");
			}
		}
        public ActionResult EditarAuto(int id)
		{
            Auto a = mngAuto.Obtener(id);
            ViewBag.marcas = mngMarca.Listar();
            return View(a);
		}

        [HttpPost]
        public ActionResult EditarAuto(Auto a)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.marcas = mngMarca.Listar();
                return View(a);
            }
            else
            {
                mngAuto.Actualizar(a);
                return RedirectToAction("ListarAutos");
            }
        }

        public ActionResult VerAuto(int id)
		{
            return View(mngAuto.Obtener(id));
		}


        public ActionResult EliminarAuto(int id)
        {
            Auto a = mngAuto.Obtener(id);
            return View(a);
        }

        [HttpPost]
        public ActionResult EliminarAuto(Auto a)
        {

                mngAuto.Eliminar(a.idAuto);
                return RedirectToAction("ListarAutos");
        }


    }
}