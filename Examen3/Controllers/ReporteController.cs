﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

// Importar
using Microsoft.Reporting.WebForms;
using Examen3.DataSets;
using Examen3.Models;

namespace Examen3.Controllers
{
    public class ReporteController : Controller
    {
        // GET: Reporte de lista de Autos
        public ActionResult ReporteAutos()
        {
            // Visor del Reporte
            ReportViewer rvw = new ReportViewer();
            rvw.ProcessingMode = ProcessingMode.Local;
            rvw.SizeToReportContent = true;
            // Indicar el nombre del reporte a mostrar en el visor
            rvw.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reportes\RptAutos.rdlc";
            // Asignamos la fuente de datos
            // 1. Importamos el modelo de datos
            BD_CL3Entities bd = new BD_CL3Entities();
            // 2. Asigna los datos de la tabla autos al Dataset que el reporte espera
            rvw.LocalReport.DataSources.Add(new ReportDataSource("DataSetAutos", bd.Vehiculo));
            // 3. Si el reporte tiene hipervinculos
            rvw.LocalReport.EnableHyperlinks = true;
            // Pasamos el visor al viewbag para que se muestre en la vista
            ViewBag.visor = rvw;

            return View();
        }
    }
}