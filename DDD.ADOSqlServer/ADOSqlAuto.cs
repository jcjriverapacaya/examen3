﻿using DDD.ADOSqlServer.Util;
using DDD.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.ADOSqlServer
{
	public class ADOSqlAuto
	{
		public List<Auto> Listar()
		{
			List<Auto> lista = new List<Auto>();

			try
			{
				SqlConnection cn = new ConexionSQL().ObtenerConexion();
				SqlCommand cmd = new SqlCommand("SP_AUTO_LISTAR", cn);
				cmd.CommandType = CommandType.StoredProcedure;

				cn.Open();
				SqlDataReader dr = cmd.ExecuteReader();
				while (dr.Read())
				{
					lista.Add(new Auto()
					{
						idAuto = dr.GetInt32(0),
						nombreM = dr.GetString(1),
						Modelo = dr.GetString(2),
						Placa = dr.GetString(3),
						Año = dr.GetInt32(4)

					});
				}

				dr.Close();
				cn.Close();
			}
			catch (Exception)
			{
				throw;
			}

			return lista;
		}

		public int Insertar(Auto a)
		{
			int resultado = -1;
			SqlConnection cn = new ConexionSQL().ObtenerConexion();

			try
			{
				SqlCommand cmd = new SqlCommand("SP_AUTO_INSERTAR", cn);
				cmd.CommandType = CommandType.StoredProcedure;

				cmd.Parameters.AddWithValue("@placa", a.Placa);
				cmd.Parameters.AddWithValue("@anio", a.Año);
				cmd.Parameters.AddWithValue("@modelo", a.Modelo);
				cmd.Parameters.AddWithValue("@idmarca", a.idMarca);

				cn.Open();
				resultado = cmd.ExecuteNonQuery();

			}
			catch
			{
				throw;
			}
			finally
			{
				cn.Close();
			}

			return resultado;
		}

		public Auto Obtener(int id)
		{
			Auto a = new Auto();
			try
			{

				SqlConnection cn = new ConexionSQL().ObtenerConexion();
				SqlCommand cmd = new SqlCommand("SP_AUTO_OBTENER", cn);
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.AddWithValue("@idauto", id);
				cn.Open();
				SqlDataReader dr = cmd.ExecuteReader();
				while (dr.Read())
				{

						a.idAuto = dr.GetInt32(0);
						a.nombreM = dr.GetString(1);
						a.Modelo = dr.GetString(2);
						a.Placa = dr.GetString(3);
						a.Año = dr.GetInt32(4);

				}

				dr.Close();
				cn.Close();
			}
			catch (Exception)
			{
				throw;
			}

			return a;
		}

		public int Actualizar(Auto a)
		{
			int resultado = -1;
			SqlConnection cn = new ConexionSQL().ObtenerConexion();

			try
			{
				SqlCommand cmd = new SqlCommand("SP_AUTO_ACTUALIZAR", cn);
				cmd.CommandType = CommandType.StoredProcedure;

				cmd.Parameters.AddWithValue("@placa", a.Placa);
				cmd.Parameters.AddWithValue("@anio", a.Año);
				cmd.Parameters.AddWithValue("@modelo", a.Modelo);
				cmd.Parameters.AddWithValue("@idmarca", a.idMarca);
				cmd.Parameters.AddWithValue("@idauto", a.idAuto);

				cn.Open();
				resultado = cmd.ExecuteNonQuery();

			}
			catch
			{
				throw;
			}
			finally
			{
				cn.Close();
			}

			return resultado;
		}


		public int Eliminar(int id)
		{
			int resultado = -1;
			SqlConnection cn = new ConexionSQL().ObtenerConexion();
			try
			{
				SqlCommand cmd = new SqlCommand("SP_AUTO_ELIMINAR", cn);
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.AddWithValue("@idauto", id);
				cn.Open();
				resultado = cmd.ExecuteNonQuery();
			}
			catch
			{
				throw;
			}
			finally
			{
				cn.Close();
			}

			return resultado;
		}




	}
}
